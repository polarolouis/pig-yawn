#!/usr/bin env python
# -*- coding: utf-8 -*-

###------------------IMPORTS-------------------------###
import tkinter as tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from pygame import mixer

import pigutils


###------------------SETTINGS WINDOW-------------------------###
settingsWindows = tk.Tk()
settingsWindows.title("Settings of the simulation")

# The scale to select the number of pigs
scaleNumberOfPigs = tk.DoubleVar()
scale = tk.Scale(settingsWindows, variable=scaleNumberOfPigs, tickinterval=50, 
orient="horizontal", from_=1, to=400, length=350,label="Number of pigs ?")
scale.pack()

differentiateSexes = False
playMusic = False
numberOfPigs = 50 # Default value in case of problems

tk.Label(settingsWindows, text="Differentiate sexes ?").pack()

valueRadio = tk.StringVar() 
button1 = tk.Radiobutton(settingsWindows, text="Yes", variable=valueRadio, value="yes")
button2 = tk.Radiobutton(settingsWindows, text="No", variable=valueRadio, value="no")
button2.select()

button1.pack()
button2.pack()

valueCheck = tk.StringVar() 
buttonMusic = tk.Checkbutton(settingsWindows, text="Mystery checkbox ?", variable=valueCheck, onvalue='yes', offvalue='no')
buttonMusic.pack()
buttonMusic.deselect()

def validate():
   """The validation function to get the selected number of pigs
   And to set the value."""
   global numberOfPigs
   global differentiateSexes
   global playMusic
   if scaleNumberOfPigs:
      # The user as selected a valid input
      numberOfPigs = int(scaleNumberOfPigs.get())
      print(f"Number of pigs : {numberOfPigs}")
      if valueRadio.get() == "yes":
         differentiateSexes = True
      if valueCheck.get() == "yes":
         playMusic = True
      print(f"Differentiate sexes : {differentiateSexes}")
      settingsWindows.destroy()
   else:
      # Display a warning tkinter window for the user to select a valid number
      tk.messagebox.showerror(title="Error !", message="Invalid Number of Pigs")
# The validation button
button = tk.Button(settingsWindows, text="Validate", command=validate)
button.pack()

settingsWindows.mainloop()


###------------------MAIN WINDOW-------------------------###
# The main simulation
mainWindow = tk.Tk()
mainWindow.title("Pig yawning propagation")

legend = tk.Frame(mainWindow, borderwidth=2, relief=tk.GROOVE)
legend.pack()

tk.Label(legend, text="Color legend:").pack()

legendCanvas = tk.Canvas(legend, height=100, width=400, bg="white")

legendCanvas.create_oval(50,10,70,30,width = 1, fill='green')
legendCanvas.create_text(60,40, text="Special pig", fill="black")
legendCanvas.create_oval(150,10,170,30,width = 1, fill='gray')
legendCanvas.create_text(160,40, text="Young pig", fill="black")
legendCanvas.create_oval(250,10,270,30,width = 1, fill='pink')
if differentiateSexes:
   legendCanvas.create_text(260,40, text="Male pig", fill="black")
   legendCanvas.create_oval(350,10,370,30,width = 1, fill='blue')
   legendCanvas.create_text(360,40, text="Female pig", fill="black")
else:
   legendCanvas.create_text(260,40, text="Pig", fill="black")

legendCanvas.create_oval(50,50,70,70,width = 1, fill='red')
legendCanvas.create_text(60,80, text="Yawning pig", fill="black")
legendCanvas.create_oval(150,50,170,70,width = 1, fill='orange')
legendCanvas.create_text(160,80, text="Pig that has yawned", fill="black")

legendCanvas.pack()

# Creation of the Canvas and adding the Piggery as an attribute
parcelSize = 420
canvas = tk.Canvas(mainWindow, height=parcelSize, width=parcelSize, bg="ivory")
canvas.pack()

canvas.piggery = pigutils.Piggery(numberOfPigs, 
   canvas=canvas, 
   parcelSize=parcelSize, 
   differentiateSexes=differentiateSexes)

paused = False

mixer.init()

def play_music():
   if playMusic:
      mixer.music.load("audio.mp3")
      mixer.music.play()

def pause_unpause():
   """Function to pause / unpause the simulation"""
   global paused
   paused = True if not paused else False
   print(f"Is paused ? : {paused}")

counterOfDays, totalYawnsCounter, spontaneousYawnsCounter, propagatedYawnsCounter = 0, 0, 0, 0

listOfDays = []
totalYawnsList = []
spontaneousYawnsList = []
propagatedYawnsList = []

def moving():
   """Function to initiate the movement"""
   global mainWindow, canvas, counterOfDays, totalYawnsCounter, spontaneousYawnsCounter, propagatedYawnsCounter, listOfDays, totalYawnsList, spontaneousYawnsList, propagatedYawnsList
   if not paused:
      canvas.piggery.move()
      canvas.piggery.yawn_propagation()
      
      # Counter and display
      counterOfDays += 1
      totalYawnsCounter = canvas.piggery.totalYawnsCounter
      spontaneousYawnsCounter = canvas.piggery.spontaneousYawnsCounter
      propagatedYawnsCounter = totalYawnsCounter - spontaneousYawnsCounter

      listOfDays.append(counterOfDays)
      totalYawnsList.append(totalYawnsCounter)
      spontaneousYawnsList.append(spontaneousYawnsCounter)
      propagatedYawnsList.append(propagatedYawnsCounter)

      textNumberDays.set(f"Number of days: {counterOfDays}")
      textTotalYawnsCounter.set(f"Total yawns this turn: {totalYawnsCounter}")
      textSpontaneousYawnsCounter.set(f"Spontaneous yawns this turn: {spontaneousYawnsCounter}")
      textPropagatedYawnsCounter.set(f"Propagated yawns this turn: {propagatedYawnsCounter}")
   mainWindow.after(1000, moving)

#Buttons
tk.Button(mainWindow, text='Begin', command=lambda: (moving(),play_music()) , bg='blue' , fg='white').pack(side= tk.LEFT, padx = 5, pady = 5)
tk.Button(mainWindow, text='Pause/Unpause', command=pause_unpause, bg='blue' , fg='white').pack(side= tk.LEFT, padx = 5, pady = 5)
tk.Button(mainWindow, text='To graph window', command=mainWindow.destroy, bg='blue' , fg='white').pack(side= tk.RIGHT, padx = 5, pady = 5)

# Display 
bottomMainWindow = tk.Frame(mainWindow, borderwidth=2, relief=tk.GROOVE)

textNumberDays = tk.StringVar()
textNumberDays.set(f"Number of days: {counterOfDays}")
tk.Label(bottomMainWindow, textvariable=textNumberDays).pack()

textTotalYawnsCounter = tk.StringVar()
textTotalYawnsCounter.set(f"Total yawns this turn: {totalYawnsCounter}")
tk.Label(bottomMainWindow, textvariable=textTotalYawnsCounter).pack()

textSpontaneousYawnsCounter = tk.StringVar()
textSpontaneousYawnsCounter.set(f"Spontaneous yawns this turn: {spontaneousYawnsCounter}")
tk.Label(bottomMainWindow, textvariable=textSpontaneousYawnsCounter).pack()

textPropagatedYawnsCounter = tk.StringVar()
textPropagatedYawnsCounter.set(f"Propagated yawns this turn: {propagatedYawnsCounter}")
tk.Label(bottomMainWindow, textvariable=textPropagatedYawnsCounter).pack()

bottomMainWindow.pack()

mainWindow.mainloop()

###------------------GRAPH WINDOW-------------------------###

graphWindow = tk.Tk()
graphWindow.title("Graph of the simulations")

figure = plt.figure()
plt.plot(listOfDays, totalYawnsList, label="Total Yawns")
plt.plot(listOfDays, propagatedYawnsList, label="Propagated Yawns")
plt.plot(listOfDays, spontaneousYawnsList, label="Spontaneous Yawns")

plt.legend()

plotInTk = FigureCanvasTkAgg(figure, graphWindow)
plotInTk.get_tk_widget().pack()

tk.Button(graphWindow, text='Quitter', command=graphWindow.quit, bg='blue' , fg='white').pack(padx = 5, pady = 5)

graphWindow.mainloop()