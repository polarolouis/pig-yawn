#%%
import math
import random

import numpy as np
import tkinter as tk
#%%
class Pig:
    """_summary_

    Returns:
        _type_: _description_
    """
    response_frequency_by_age = {9: 0.52, 10:0.05, 11:0.1, 12:0.15, 
        13:0.45, 14:0.4, 15:0.2, 16:0.13, 17:0.13, # Added 17 to permit the program to run
        18:0.8, 19:0.82, 20:0.68, 21:0.25, 22:0.6, 
        23:0.6} # Added 23 just in case
    


    def __init__(self, age : int, sex : str, canvas : tk.Canvas, pigRadius, differentiateSexes,x=0, y=0, isSpecialPig = False) -> None:
        self.canvas = canvas        
        self.isSpecialPig = isSpecialPig
        self.age = age
        self.daysAlive = self.age * 30
        # Pig sex
        if sex == "random":
            sex, *_ = random.choices(["male", "female"], weights=[1, 1], k=1)
        self.sex = sex
        self.differentiateSexes = differentiateSexes
        # Yawn information
        self.daysSinceHasYawned = 0
        self.willYawn = False
        self.yawning = False
        self.hasYawned = False
        self.probabilityToYawn = 0
        self.yawnTransmissionProbabilityBySex = 0.4 if self.sex == "male" else 0.28 
        # Coordinates
        self.x = x
        self.y = y
        # Start with a random direction
        self.direction = random.choice(["up", "down", "left", "right"])
        self.pigRadius = pigRadius
        self.paceSize = 2*self.pigRadius
        x1,y1,x2,y2 = self.bbox()
        if self.isSpecialPig:
            self.id = self.canvas.create_oval(x1,y1,x2,y2,width = 1, fill='green')
        elif self.age >= 9:
            if self.sex == "female" and self.differentiateSexes:
                self.id = self.canvas.create_oval(x1,y1,x2,y2,width = 1, fill='blue')
            else:
                self.id = self.canvas.create_oval(x1,y1,x2,y2,width = 1, fill='pink')
        else:
            self.id = self.canvas.create_oval(x1,y1,x2,y2,width = 1, fill='gray')

    def __str__(self) -> str:
        return f"ID : {self.id} | {self.sex.capitalize()} pig, {self.age} months old and has been in the pig farm for {self.daysAlive} days\n{self.x, self.y}"
    
    def __set_age(self, age):
        """Function to check if age is valid and set it to its value or default to 0 months
        """
        if 0 < age <= 23:
            self.__age = age
        else:
            self.__age = 0
    
    def __get_age(self):
        """Retrieves the age of the pig"""
        return self.__age

    age = property(fget=__get_age, fset=__set_age)

    def bbox(self):
        """Method to return the top left point and the bottom right point's coords"""
        return [self.x-self.pigRadius, self.y-self.pigRadius, self.x+self.pigRadius, self.y+self.pigRadius]

    def can_yawn(self):
        """Return a boolean if pig age is over 9 months"""
        return self.age >= 9 and self.daysSinceHasYawned == 0
    
    def old_enough_to_yawn(self):
        return self.age >=9
    
    def yawn(self):
        self.yawning = True
        self.canvas.itemconfigure(self.id, fill='red')
        self.hasYawned = True
    
    def is_at_reproductive_age(self):
        """Checks if the pig is old enough to reproduce"""
        return self.age >=23

    def reproduce(self, reproduction_is_possible : bool):
        """Method to implement reproduction.
        SHOULD BE CALLED ONCE IT HAS BEEN CHECKED THAT REPRODUCTION WAS POSSIBLE
        The Pig should die after reproduction."""
        if (self.is_at_reproductive_age() and reproduction_is_possible): 
            return Pig(0, "random", self.x, self.y, differentiateSexes=self.differentiateSexes) 
    
    def has_a_month_passed(self):
        """Test if 30 days have passed"""
        return not self.daysAlive % 30

    def add_month_to_age(self):
        """Increment age until the pig turns 23 and block after"""
        if self.age <= 22:
            self.age += 1

    def move(self, list_of_possible_moves : list):
        """Determine the movement of the pig"""
        self.daysAlive += 1 # Pig moves one time a day
        max_size_canvas = int(self.canvas.cget('width'))
        if list_of_possible_moves:
            if self.direction in list_of_possible_moves and random.random() < 0.8:
                # If the pig can continue to go in the same direction it has 80% chance to continue
                self.direction = self.direction
            else:
                self.direction = random.choice(list_of_possible_moves)
        dx, dy = 0, 0
        if self.direction == 'up':
            dy = -self.paceSize
        elif self.direction == 'down':
            dy = self.paceSize
        elif self.direction == 'left':
            dx = -self.paceSize
        elif self.direction == 'right':
            dx = self.paceSize
        else:
            print("Invalid direction !")
        # Verify if the pig we'll move inside the canvas
        if self.x + dx > max_size_canvas - self.pigRadius:
            # If we hit the wall the pig stops
            dx = 0
        if self.x + dx < self.pigRadius:
            dx = 0

        if self.y + dy > max_size_canvas - self.pigRadius:
            # If we hit the wall the pig stops
            dy = 0
        if self.y + dy < self.pigRadius:
            dy = 0
        x1,y1,x2,y2 = self.bbox() # retrieves points coords
        if self.direction in list_of_possible_moves:
            # If we can move along the direction, we move the pig
            if self.isSpecialPig:
                print(f"Moving in {self.direction}")
            self.canvas.coords(self.id, x1+dx, y1+dy, x2+dx, y2+dy)
            self.x += dx
            self.y += dy
            self.hasMoved = True
        if self.hasYawned: # If the pig has yawned, the days since increment
            self.daysSinceHasYawned += 1
        
        if self.has_a_month_passed():
            self.add_month_to_age()

class Piggery:
    def __init__(self, numberOfPigs : int, canvas : tk.Canvas, parcelSize : int = 400, sexRatio=0.5, differentiateSexes = False) -> None:
        
        self.pigsList = []
        self.canvas = canvas
        
        # Coordinates calculations
        self.parcelSize = parcelSize # Size of the square
        self.parcelArea = self.parcelSize**2
        self.maxAreaPerPig = 400
        self.radiusOfPig = 10
        self.maxNumberOfPigs = (self.parcelSize/(2*self.radiusOfPig))**2
        self.spontaneousYawnProbability = 0.001
        self.paceSize = 2*self.radiusOfPig

        # Death
        self.pigs_to_kill = []

        # Yawn
        self.yawning_pigs = []
        self.differentiateSexes = differentiateSexes

        self.spontaneousYawnsCounter = 0
        self.totalYawnsCounter = 0


        if not 1 <= numberOfPigs <= self.maxNumberOfPigs:
            # If the number of pigs is not right we use randint to generate 
            # a number that fits our conditions
            
            numberOfPigs = random.randint(1, self.maxNumberOfPigs)
            print(f"Invalid number of pigs should be max {self.maxNumberOfPigs}")
        
        self.possible_grid_positions = [] # theoretical pigs (an idea to develop)
        
        specialPigId = random.randint(0,numberOfPigs-1)

        for y in np.arange(self.paceSize , self.parcelSize-self.radiusOfPig, self.paceSize):
        # We go through the columns of the parcel
            for x in np.arange(self.paceSize, self.parcelSize-self.radiusOfPig, self.paceSize):
                # We go through the rows of the parcel
                self.possible_grid_positions.append((x,y))

        counter = 0

        for i,(x,y) in enumerate(random.sample(self.possible_grid_positions, k=numberOfPigs, )):
            age = random.randint(6,22)
            sex, *_ = random.choices(["male", "female"], weights=[sexRatio*100, (1-sexRatio)*100], k=1)
            if i == specialPigId:
                print(f"The special pig is {specialPigId}")
                self.pigsList.append(Pig(age, sex, self.canvas, self.radiusOfPig, self.differentiateSexes, x=x, y=y, isSpecialPig=True))
            else:
                self.pigsList.append(Pig(age, sex, self.canvas, self.radiusOfPig, self.differentiateSexes, x=x, y=y))
            counter += 1
    
        print(f"{i} pigs created")
    
    def __iter__(self):
        return (pig for pig in self.pigsList)

    def __len__(self):
        return len(self.pigsList)

    def move(self):
        for pig in self.pigsList:
            list_of_possible_moves = self.check_where_pig_can_go(pig)
            #print(f"{pig.id} can go {list_of_possible_moves}")
            pig.move(list_of_possible_moves)

    def check_where_pig_can_go(self, pig):
        """This method uses the bbox of a pig to check if he has neighbours 
        and returns a list of possible movements"""
        x1,y1,x2,y2 = pig.bbox()
        detectionValue = 0.3
        x1 = x1 + detectionValue * self.paceSize
        y1 = y1 + detectionValue * self.paceSize
        x2 = x2 - detectionValue * self.paceSize
        y2 = y2 - detectionValue * self.paceSize
        
        if pig.isSpecialPig:
            print(f"---------\nChecking where {pig.id} with coords {(x1,y1),(x2,y2)} can go:")
        
        list_possible_directions = []
        
        if pig.isSpecialPig:
            print(f"Left is : {self.canvas.find_overlapping(x1-self.paceSize,y1,x2-2*self.radiusOfPig,y2)} ")
        if self.canvas.find_overlapping(x1-self.paceSize,y1,x2-self.paceSize,y2) == () :
            # If we can go left and there is no overlapping pig on the left
            list_possible_directions.append('left')
        
        if pig.isSpecialPig:
            print(f"Right is : {self.canvas.find_overlapping(x1+self.paceSize,y1,x2+self.paceSize,y2)}")
        if self.canvas.find_overlapping(x1+self.paceSize,y1,x2+self.paceSize,y2) == ():
            # If we can go right and there is no overlapping pig on the right
            list_possible_directions.append('right')
        
        if pig.isSpecialPig:
            print(f"Up is : {self.canvas.find_overlapping(x1=x1, y1=y1-self.paceSize, x2=x2, y2=y2-self.paceSize)}")
        if self.canvas.find_overlapping(x1=x1, y1=y1-self.paceSize, x2=x2, y2=y2-self.paceSize) == ():
            # If we can go up and there is no overlapping pig up, we go up
            list_possible_directions.append('up')
        
        if pig.isSpecialPig:
            print(f"Down is : {self.canvas.find_overlapping(x1=x1, y1=y1+self.paceSize, x2=x2, y2=y2+self.paceSize)}")
        if self.canvas.find_overlapping(x1=x1, y1=y1+self.paceSize, x2=x2, y2=y2+self.paceSize) == ():
            # If we can go down and there is no overlapping pig down, we go down
            list_possible_directions.append('down')
        
        if pig.isSpecialPig:
            print(f"Possible movements :{list_possible_directions}")
        return list_possible_directions

    def spontaneous_yawn(self):
        """Method to generate spontaneous yawns"""
        for pig in self.pigsList:
            if (random.random() < self.spontaneousYawnProbability) and pig.old_enough_to_yawn():
                self.spontaneousYawnsCounter += 1
                pig.willYawn = True

    def yawn_propagation(self):
        """Calculcates the yawn propagation within the piggery and applies it"""
        # Resets the counters for this turn
        self.spontaneousYawnsCounter = 0
        self.spontaneous_yawn()
        self.totalYawnsCounter = 0
        # Change colors according to the states of the pigs and alter
        # the states
        for pig in self.pigsList:
            if pig.age >= 9:
                if pig.sex == "female" and self.differentiateSexes:
                    pig.canvas.itemconfigure(pig.id, fill='blue')
                elif pig.isSpecialPig:
                    pig.canvas.itemconfigure(pig.id, fill='green')
                else:
                    pig.canvas.itemconfigure(pig.id, fill='pink')
            if pig.willYawn:
                pig.yawn()
                pig.willYawn = False
            if pig.yawning:
                self.yawning_pigs.append(pig)
                self.totalYawnsCounter += 1
                pig.yawning = False
                pig.canvas.itemconfigure(pig.id, fill='red')
            if pig.hasYawned and 0 < pig.daysSinceHasYawned < 4:
                pig.canvas.itemconfigure(pig.id, fill='orange')
            if pig.hasYawned and pig.daysSinceHasYawned == 4:
                # Resets the hasYawned state
                # Can yawn again
                pig.hasYawned = False
                pig.daysSinceHasYawned = 0
                if pig.sex == "female" and self.differentiateSexes:
                    pig.canvas.itemconfigure(pig.id, fill='blue')
                elif pig.isSpecialPig:
                    pig.canvas.itemconfigure(pig.id, fill='green')
                else:
                    pig.canvas.itemconfigure(pig.id, fill='pink')
        
        for pig in self.pigsList:
        # Determines if the yawn will propagate
            if pig.can_yawn():
                pig.probabilityToYawn = 0 # For each pig we reset its probability to yawn
                                            # And during the loop we'll increase it to take into 
                                            # account all the contributions of the surrounding pigs
                for yawning_pig in self.yawning_pigs:    
                    distance = math.dist((yawning_pig.x, yawning_pig.y), (pig.x, pig.y))
                    distancePropagationRatio = 1
                    if distance == 0:
                        continue # it's the yawning pig itself
                    elif distance < self.paceSize*distancePropagationRatio*1:
                        pig.probabilityToYawn += yawning_pig.yawnTransmissionProbabilityBySex * 0.65 * pig.response_frequency_by_age[pig.age]
                    elif self.paceSize*distancePropagationRatio*1 <= distance <= self.paceSize*distancePropagationRatio*10:
                        pig.probabilityToYawn += yawning_pig.yawnTransmissionProbabilityBySex * 0.2 * pig.response_frequency_by_age[pig.age]
                    elif self.paceSize*distancePropagationRatio*1 <= distance <= self.paceSize*distancePropagationRatio*12:
                        pig.probabilityToYawn += yawning_pig.yawnTransmissionProbabilityBySex * 0.25 * pig.response_frequency_by_age[pig.age]

                if (random.random() < pig.probabilityToYawn):
                    pig.willYawn = True

    def to_be_killed(self, pig):
        """Class method to mark the pigs to be killed
        SHOULD BE USED WITH A CONDITION TO MARK THE PIGS"""
        if isinstance(pig, Pig):
            self.pig.canvas
            self.pigs_to_kill.append(pig)
        else:
            raise TypeError("the pig variable is not a Pig object !")
    
    def kill(self):
        """Method to kill the pigs marked by self.to_be_killed"""
        for pig_to_kill in self.pigs_to_kill:
            self

# %%
