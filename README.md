# Manuel utilisateur

## Fonctionnalitées développées
- **Sélection par interface graphique du nombre de cochons**
- **Démarrage, mise en pause, reprise et fin de la simulation par interface graphique**
- Génération d'une porcherie et affichage
- Génération de chaque cochon et affichage
- Répartition aléatoire des cochons dans la porcherie
- Déplacement dynamique des cochons avec système anti-collision et anti-recouvrement
- Modélisation du baillement et de sa transmission selon les spécifications du sujet du projet
- Affichage dynamique du baillement et des différents états de baillements
- Prise en compte de l'âge des porcs et affichage dynamique
- Possibilité de différencier visuellement le sexe des porcs et **sélection avec un bouton**
- Possibilité de jouer une musique audio.mp3
- Légende pour identifier les états des cochons
- Légende pour comptabiliser le nombre de jours et les nombres de bâillements des cochons
- Fenêtre graphique pour accéder à un graphe des chroniques des bâillements.

## Usage du programme
1. Exécuter le code à l'aide de son interpréteur Python ou de son IDE
2. Choisir le nombre de cochon à mettre dans la simulation grâce à la barre de défilement
3. Régler les options de différenciation du sexe et de musique (Mystery Checkbox)
4. Valider en pressant "Validate"

5. Démarrer la simulation en appuyant sur "Begin"
6. Mettre en pause/Reprendre la simulation avec le bouton "Pause/Unpause"
7. Passer à la fenêtre du graphique des bâillements en appuyant sur le bouton "To the graph window"
8. Quitter le programme en appuyant sur le bouton "Quitter" (*qui devrait être traduit en anglais mais nous avons visiblement oublié...*)

9. Pour relancer une simulation revenir à l'étape **1**

